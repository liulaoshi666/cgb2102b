package com.jt.service;

import com.jt.mapper.UserMapper;
import com.jt.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("target")
public class UserServiceImpl implements UserService{

    @Autowired
    private UserMapper userMapper;

    //事务控制应该放到Service层中进行控制
    @Override
    public void addUser(User user) {
        userMapper.addUser(user);
    }

    @Override
    public void deleteUser(User user) {
        userMapper.deleteUser(user);
    }


    /*//事务控制应该放到Service层中进行控制
    @Override
    public void addUser(User user) {
        try {
            System.out.println("Spring事务开始");
            userMapper.addUser(user);
            System.out.println("事务结束");
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("事务回滚");
        }
    }*/

}

package com.jt.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration  //标识我是一个配置类
@ComponentScan("com.jt")
public class SpringConfig {

}



package com.jt.test;

import com.jt.config.SpringConfig;
import com.jt.proxy.JDKProxyFactory;
import com.jt.service.DeptService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestDept {

    @Test
    public void testTx(){
        //1.获取目标对象
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        DeptService target = (DeptService) context.getBean("deptService");
        //2.获取代理对象
        DeptService deptService = (DeptService) JDKProxyFactory.getProxy(target);
        //通过代理对象 调用方法  扩展了方法!!!!!
        deptService.addDept();  //invoke
    }
}

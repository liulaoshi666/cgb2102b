package com.jt.test;

import com.jt.config.SpringConfig;
import com.jt.pojo.User;
import com.jt.proxy.JDKProxyFactory;
import com.jt.service.UserService;
import com.jt.service.UserServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestUser {


    /**
     * Controller-Service-Mapper(Dao)
     * Spring中规定:
     *         如果传入的是接口的类型 则自动查找/注入 该接口的实现类
     *         该接口只有一个实现类
     * 注入接口的原理:
     *          if(getBean(isinterface)){
     *              Class targetClass = interface.getImpl();
     *              根据类型,动态获取对象
     *              return 对象
     *          }
     */
    @Test
    public void testTx(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        //写接口类型/实现类的类型?   1.能通  2.不同通
        // 向上造型
        UserService userService = context.getBean(UserService.class);
        //UserService userService = (UserService) context.getBean("userServiceImpl");
        User user = new User();
        user.setId(101);
        user.setName("SpringAOP测试入门案例");
        userService.addUser(user);
    }

    @Test
    public void testStaticProxy(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        UserService userService = (UserService) context.getBean("userService");
        User user = new User();
        user.setId(10001);
        user.setName("测试代理机制");
        //执行用户调用
        userService.addUser(user);
    }

    /**
     * 测试JDK动态代理
     */
    @Test
    public void testJDKProxy(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        //1.获取用户目标对象
        UserService target = (UserService) context.getBean("target");
        //2.获取代理对象
        UserService userService = (UserService) JDKProxyFactory.getProxy(target);
        //3.打印代理对象的类型
        System.out.println(userService.getClass());
        //4.用户完成调用
        User user = new User();
        user.setId(1001);
        user.setName("JDK动态代理完成");
        //新增用户方法
        userService.addUser(user);
        //动态代理对方法进行优化
        //删除用户的方法
        userService.deleteUser(user);
    }










}

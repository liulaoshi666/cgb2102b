package com.jt.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jt.mapper.ItemCatParamMapper;
import com.jt.pojo.ItemCatParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ItemCatParamServiceImpl implements ItemCatParamService{

    @Autowired
    private ItemCatParamMapper itemCatParamMapper;

    //Sql:select * from item_cat_param where item_cat_id = xxx and param_type=1
    @Override       //参数中只有2个数据不为null
    public List<ItemCatParam> findParamListByType(ItemCatParam itemCatParam) {
        //QueryWrapper<ItemCatParam> queryWrapper = new QueryWrapper<>(itemCatParam);
        return itemCatParamMapper.selectList(new QueryWrapper<>(itemCatParam));
    }

    @Override
    @Transactional  //事务控制
    public void addItemCatParam(ItemCatParam itemCatParam) {

        itemCatParamMapper.insert(itemCatParam);
    }

    @Override
    @Transactional
    public void updateItemCatParam(ItemCatParam itemCatParam) {

        itemCatParamMapper.updateById(itemCatParam);
    }

    @Override
    public void deleteItemCatParam(Integer paramId) {

        itemCatParamMapper.deleteById(paramId);
    }
}

package com.jt.aop;

import com.jt.vo.SysResult;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.io.IOException;

@RestControllerAdvice   //定义全局异常处理 拦截controller层 返回JSON
public class AOPException {

    //实现原理 异常通知
    //1.拦截什么类型的异常!!
    //2.拦截之后如何处理!!
    @ExceptionHandler({RuntimeException.class})
    public Object exception(Exception e){
        //将异常 控制台输出
        e.printStackTrace();
        return SysResult.fail();
    }
}

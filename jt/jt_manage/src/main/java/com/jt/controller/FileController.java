package com.jt.controller;

import com.jt.service.FileService;
import com.jt.vo.ImageVO;
import com.jt.vo.ItemVO;
import com.jt.vo.SysResult;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@RestController
@CrossOrigin
@RequestMapping("/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @PostMapping("/upload")
    public SysResult upload(MultipartFile file) throws IOException {

        ImageVO imageVO = fileService.upload(file);
        if(imageVO == null){
            return SysResult.fail();
        }
        return SysResult.success(imageVO);
    }

    //实现文件的删除功能
    //URL: /file/deleteFile
    @DeleteMapping("/deleteFile")
    public SysResult fileRemove(String virtualPath){

        fileService.fileRemove(virtualPath);
        return SysResult.success();
    }








    /**
     * Demo
     * URL:http://localhost:8091/file/upload
     * 参数: file:二进制信息
     * 返回值: SysResult对象
     * MultipartFile:SpringMVC 对外提供的接口 专门实现文件上传操作
     * 高级API 默认的文件大小 最大1M
     * 如果需要优化则需要编辑配置类 重新定义大小(一般不这么做)
     * 优化:  1.防止文件重名
     *        2.防止恶意程序 jpg|png|gif
     */
    /*@PostMapping("/upload")
    public SysResult upload(MultipartFile file) throws IOException {
        //1.获取文件名称
        String fileName = file.getOriginalFilename();
        //2.准备文件上传的本地目录
        String fileDir = "D:/JT_IMAGE/";
        //3.是否需要判断目录是否存在
        File filePath = new File(fileDir);
        if(!filePath.exists()){
            //可以创建多级目录
            filePath.mkdirs();
            //只创建一级目录
            //filePath.mkdir();
        }

        //4.准备输出的对象 文件的全路径="文件目录"/+"文件的名称"
        String realFilePath = fileDir + fileName;
        File realFile = new File(realFilePath);
        //5.实现文件上传
        file.transferTo(realFile);
        return SysResult.success();
    }*/
}

package com.jt.controller;

import com.jt.pojo.Item;
import com.jt.service.ItemService;
import com.jt.vo.ItemVO;
import com.jt.vo.PageResult;
import com.jt.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    /**
     * 实现商品列表展现
     * URL: /item/getItemList?query=&pageNum=1&pageSize=10
     * 参数: 使用分页参数
     * 返回值: SysResult对象(pageResult对象)
     */
    @GetMapping("/getItemList")
    public SysResult findItemList(PageResult pageResult){

        //查询分页数据 返回分页对象
        pageResult = itemService.findItemList(pageResult);
        return SysResult.success(pageResult);
    }

    /**
     * url地址: /item/updateItemStatus
     * 请求参数:  利用Item对象接收
     *           id: item.id,
     *           status: item.status
     * 返回值: SysResult对象
     */
    @PutMapping("/updateItemStatus")
    public SysResult updateItemStatus(@RequestBody Item item){

        itemService.updateItemStatus(item);
        return SysResult.success();
    }


    /**
     * 商品新增
     * URL: /item/saveItem
     * 参数: {Item,itemDesc,ItemParam} 使用ItmeVO接收
     * 返回值: SysResult
     */

    @PostMapping("/saveItem")
    public SysResult saveItem(@RequestBody ItemVO itemVO){

        itemService.saveItem(itemVO);
        return SysResult.success();
    }


}

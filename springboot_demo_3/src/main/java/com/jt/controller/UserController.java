package com.jt.controller;

import com.jt.pojo.Dog;
import com.jt.pojo.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

@Controller
public class UserController {

    /**
     * 测试转化和重定向
     * 1.准备一个请求 findUser请求.
     * 2.要求通用转发到 findDog请求中.
     *
     * 3.关键字   forward: 转发的是一个请求.....
     *           redirect: 多次请求多次响应
     * 4.特点:
     *      1.转发时 会携带用户提交的数据.
     *      2.转发时 用户浏览器的地址不会发生改变.
     *      3.重定向时  由于是多次请求,所以不会携带用户的数据
     *      4.重定向时  由于是多次请求,所以用户的浏览器的地址会发生变化
     *
     * 5.实际意义: 实现了方法内部的松耦合
     * 6.什么时候使用转发/什么时候使用重定向
     *   1.如果需要携带参数 使用转发
     *   2.如果一个业务已经完成需要一个新的开始 则使用重定向
     */
    @RequestMapping("/findUser")
    public String findUser(String name){

        //return 本身就是一个转发
        //return "user1";
        //return "dog"; 页面耦合性高
        //return "forward:/findDog";   转发到findDog请求
        return "redirect:/findDog";    //重定向到findDog请求
    }

    //需要将name属性返回给页面
    @RequestMapping("/findDog")
    public String findDog(String name,Model model){
        System.out.println("动态获取name属性值:"+name);
        model.addAttribute("name",name+"旺旺旺");
        return "dog";
    }







    /**
     * 在内部封装引用对象. 使用User接收全部数据.
     * @param user
     * @return
     */
    @RequestMapping("/addUser")
    public String addUserDog(User user){

        System.out.println(user);
        return "success"; //转发
    }


    /**
     * 使用对象的方式接收数据
     * URL地址: /addUser
     * url参数: id: name:  hobbys: 敲代码 hobbys: 敲键盘 hobbys: 敲主机
     * 对象赋值的原理:
     *      要求: POJO对象中必须有get/set方法
     *      1. 当用户提交数据之后,利用对象的set方法为属性赋值.
     *
     * @return
     */
    //@RequestMapping("/addUser")
    public String addUser(User user){

        System.out.println(user);
        return "success";
    }




    /**
     * 同名提交测试
     * url参数: id: name:  hobbys: 敲代码 hobbys: 敲键盘 hobbys: 敲主机
     * 参数提交的形式:springMVC自动的将参数进行了","号拼接  敲键盘,敲主机
     *
     * SpringMVC优化:
     *   1.可以根据,号 自动的将字符串进行拆分
     *   2.如果数据类型不是String类型,则可以自动的转化
     *
     * 总结: 如果以后遇到了同名提交问题.则使用 数组 或者 可变参数 类型接收
     * String... hobbys 可变参数类型 实质就是数组
     */
    //@RequestMapping("/addUser")
    public String addHobbys(Integer id,String name,String[] hobbys){

        System.out.println("参数获取:"+id+":"+name+":"+ Arrays.toString(hobbys));
        return "success";
    }





    /**
     * 请求参数: id: 100  name: 张三
     * 测试@RequestParam注解
     * @RequestParam 参数说明:
     *  1.name/value   接收参数的名称
     *  2.required     默认值true  该数据项为必填项
     *  3.defaultValue 设定数据默认值  如果参数为null则设定默认值
     *  required与defaultValue 是互斥的
     */
    //@RequestMapping("/addUser")
    public String addUserParam(
        @RequestParam(value = "id",required = true,defaultValue = "1001") Integer id,
        @RequestParam(value="name",required = true,defaultValue = "张三") String name){
        System.out.println("参数获取:"+id+":"+name);
        return "success";
    }




    /**
     * 请求路径: http://localhost:8090/addUser
     * 请求参数: id: 100  name: 张三
     * request/response对象说明  只要用户调用就会自动的赋值
     * servlet缺点:  接收的参数都是String类型
     * SpringMVC赋值:
     *      内部根据request.getParameter("id") 方式获取数据.
     * @return
     */
    //@RequestMapping("/addUser")
    public String addUser(Integer id, String name){
        System.out.println("参数获取:"+id+":"+name);
        return "success";
    }


    //@RequestMapping("/addUser")
    public String addUser(HttpServletRequest request){
        //利用工具API进行类型转化
        Integer id = Integer.parseInt(request.getParameter("id"));
        String name = request.getParameter("name");
        System.out.println("参数:"+id+":"+name);

        return "success";
    }




    //简化数据传递
    @RequestMapping("/user")
    public String toUser(Model model){
        //将数据通过model进行传递
        model.addAttribute("id", 1003);
        model.addAttribute("name", "SpringMVC");
        return "user";
    }


   /**
     *  mvc底层数据传输原则
     *  url: http://localhost:8090/user
     *  ModelAndView:
     *      1.model 封装数据的
     *      2.View  封装视图页面的
     *
     *  handler处理器真正的执行时 才会调用方法
     */
   /* @RequestMapping("/user")
    public ModelAndView toUser(){
        ModelAndView modelAndView = new ModelAndView();
        //封装数据
        modelAndView.addObject("id", 1001);
        modelAndView.addObject("name", "安琪拉");
        //封装页面数据
        modelAndView.setViewName("user"); //前缀/后缀
        return modelAndView;
    }*/



}

package com.jt;

import com.jt.config.SpringConfig;
import com.jt.controller.UserController;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class TestSpring {

    @Test
    public void test01(){
        ApplicationContext context =
                new ClassPathXmlApplicationContext("application_1.xml");
        //UserController userController = context.getBean(UserController.class);
        UserController userController =
                (UserController) context.getBean("userController");
        userController.addUser();
    }

    //通过配置类测试代码
    @Test
    public void testAnno(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        UserController userController = context.getBean(UserController.class);
        userController.addUser();
    }






}

package com.jt;

import com.jt.config.SpringConfig;
import com.jt.proxy.JDKProxyFactory;
import com.jt.service.UserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class TestSpring {

    @Test
    public void test01(){
        ApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);
        //1.获取目标对象
        UserService target = (UserService) context.getBean("target");
        //2.获取代理对象
        UserService proxy = (UserService) JDKProxyFactory.getProxy(target);
        System.out.println(proxy.getClass());
        //3.调用业务方法
        proxy.addUser();
    }
}

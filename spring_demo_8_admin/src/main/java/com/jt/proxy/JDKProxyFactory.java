package com.jt.proxy;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JDKProxyFactory {

    //编辑静态方法获取代理对象
    public static Object getProxy(final Object target){
        //3个参数  1.类加载器  2.对象的接口
        ClassLoader classLoader = target.getClass().getClassLoader();
        Class[] interfaces = target.getClass().getInterfaces();
        Object proxy = Proxy.newProxyInstance(classLoader, interfaces,
                new InvocationHandler() {
                    //代理对象执行目标方法时执行
                    @Override
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                        //让用户执行目标方法
                        Long startTime = System.currentTimeMillis(); //开始时间
                        //执行目标方法 获取返回值 可能为null
                        Object result = method.invoke(target);
                        Long endTime = System.currentTimeMillis();  //结束时间
                        //根据项目经理要求 给程序预留bug 后期维护时删除 不友好
                        Thread.sleep(2000);
                        System.out.println("程序执行:"+(endTime-startTime)+"毫秒");
                        //将返回值传递给调用者
                        return result;
                    }
                })  ;

        return proxy;
    }


}

package com.jt.aop;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Order(2)
public class Before1 {

    @Before("@annotation(com.jt.anno.Cache)")
    public void before(){
        System.out.println("我是切面A执行");
    }
}

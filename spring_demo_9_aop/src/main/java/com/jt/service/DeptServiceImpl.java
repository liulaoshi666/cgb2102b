package com.jt.service;

import com.jt.anno.Cache;
import org.springframework.stereotype.Service;

@Service
public class DeptServiceImpl implements DeptService{

    @Override
    public void addDept() {
        System.out.println("添加部门信息");
    }

    @Override
    @Cache      //被注解标识
    public void updateDept() {
        System.out.println("更新部门");
    }

    @Override
    @Cache  //标识该方法需要执行切面
    public String after(Integer id) {

        return "Spring通知的测试";
    }

    //让该方法执行时 抛出异常
    @Override
    @Cache
    public void afterThrow() {
        System.out.println("用户执行目标方法");
        //手动抛出算数异常
        int a  = 1/0;
    }

    @Override
    @Cache  //标识执行AOP中的方法
    public void doAround() {
        System.out.println("实现用户数据的入库操作");
    }

    @Override
    @Cache
    public void doOrder() {
        System.out.println("测试程序执行的顺序");
    }
}

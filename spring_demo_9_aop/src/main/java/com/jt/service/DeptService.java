package com.jt.service;

public interface DeptService {

    void addDept();
    void updateDept();
    //AOP中的测试方法
    String after(Integer id);
    //测试异常通知
    void afterThrow();
    //测试环绕通知执行
    void doAround();
    //测试执行的顺序
    void doOrder();
}

package com.jt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jt.pojo.DemoUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
/*注意事项: basemapper必须添加泛型对象 切记!!!*/
public interface DemoUserMapper extends BaseMapper<DemoUser> {
    //使用MP不要重载里边的方法 容易解析异常
    List<DemoUser> findAll();

    void insertUser(DemoUser user);

    void updateUser(String oldName, String nowName, String sex);
}
